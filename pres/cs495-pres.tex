\documentclass{beamer}
\usepackage[latin1]{inputenc}
\usepackage{tikz}
\usetheme{Frankfurt}
\usecolortheme{seahorse}
\title[Approximate Finite Automata]{Design of Approximately Correct Finite Automata for Non-regular Languages}
\author{Jacob Combs\\Advisor: Dr.\ Ravikumar}
\institute{Sonoma State University}
\date{April 31, 2014}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}

\section{Theory of Computation Review}

\subsection{Strings, alphabets, and languages}

\begin{frame}
%\begin{itemize}
%\item
An \alert{alphabet} is a set of symbols
\begin{itemize}
\item
${ \{\texttt{a},\texttt{b},\cdots,\texttt{z}\} }$
\item
${ \{\texttt{0},\texttt{1}\} }$
\item
${ \left\{\begin{pmatrix}0\\0\end{pmatrix},
\begin{pmatrix}0\\1\end{pmatrix},
\begin{pmatrix}1\\0\end{pmatrix},
\begin{pmatrix}1\\1\end{pmatrix} \right\} }$
\end{itemize}
%\item
A \alert{string} is a sequence of symbols
\begin{itemize}
\item
\texttt{Ravikumar}
\item
\texttt{00101}
\item
${ \begin{pmatrix}0\\0\end{pmatrix}
\begin{pmatrix}0\\1\end{pmatrix}
\begin{pmatrix}0\\1\end{pmatrix} }$
\end{itemize}
%\end{itemize}
\end{frame}

\begin{frame}
%\begin{itemize}
%\item
A \alert{language} is a set of strings (``over'' an alphabet)
\begin{itemize}
\item
all bit strings ending in \texttt{011}
\item
strings over ${ \left\{\texttt{a},\texttt{b}\right\} }$ containing an odd number of \texttt{a} symbols
\item
strings of the form
\[
\begin{pmatrix}a_n\\b_n\\c_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\\c_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_1\\b_1\\c_1\end{pmatrix}
\]
where ${ a_i,b_i,c_i\in\{0,1\} }$, and the numbers $a$, $b$, and $c$ represented by each row (i.e.\ ${ a=a_na_{n-1}\ldots{}a_1 }$, and so on)
%, ${ b=b_nb_{n-1}\ldots{}b_1 }$, and ${ c=c_nc_{n-1}\ldots{}c_1 }$ 
satisfy ${ a+b=c }$
\end{itemize}
%\item
A language is \alert{regular} if some deterministic finite automata (DFA) ``recognizes'' it
%\end{itemize}
\end{frame}

\subsection{Deterministic finite automata and regular languages}

\begin{frame}
Example DFA to recognize bit strings ending in \texttt{011}:
\includegraphics[width=\linewidth]{dfa-example1}

Notes about DFAs:
\begin{itemize}
\item
Outgoing transitions for each input, from each state
\item
States marked as ``accepting'' or not
\end{itemize}
\end{frame}

\begin{frame}
Consider a language made up of strings of the form
\[
\begin{pmatrix}a_n\\b_n\\c_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\\c_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_1\\b_1\\c_1\end{pmatrix}
\]
where ${ a_i,b_i,c_i\in\{0,1\} }$, and the numbers $a$, $b$, and $c$ represented by each row (i.e.\ ${ a=a_na_{n-1}\ldots{}a_1 }$, and so on)
%, ${ b=b_nb_{n-1}\ldots{}b_1 }$, and ${ c=c_nc_{n-1}\ldots{}c_1 }$ 
satisfy ${ a\times{}b=c }$.

\vspace{1cm}
It can be shown that \textbf{this language is not regular}.
\end{frame}

\section{Problem Statement}

\subsection{Question}

\begin{frame}
\textbf{Question: Can we use DFA to solve multiplication problems approximately?}

\vspace{0.5cm}
Consider a simpler language $L$ containing strings of the form
\[
\begin{pmatrix}a_n\\b_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_1\\b_1\end{pmatrix}
\]
where ${ a_i,b_i,c_i\in\{0,1\} }$, and the numbers $a$ and $b$ represented by each row yield a $2n$-bit product.

\vspace{0.5cm}
(Recall that if $a$ and $b$ have $n$ and $m$ digits respectively, then $ab$ has either $nm$ or $(nm-1)$ digits.)
\end{frame}

\subsection{Solution outline}

\begin{frame}
Approach:
\begin{enumerate}
\item
Fix a number of states $N$
\item
Calculate probabilities ${ P(s\in{}L\;|\;\text{input so far}) }$
\item
Using these probabilities, allocate states to reasonable subproblems
\end{enumerate}
\end{frame}

\section{A Solution}

\subsection{State Allocation Algorithm}

\begin{frame}{Procedure: BuildDFA}
%Procedure: BuildDFA \\
Input:
\begin{itemize}
\item $M$, an incomplete DFA; $M$ will initially have 3 states: \textbf{start}, \textbf{accept} (absorbing), and \textbf{reject} (absorbing)
\item $q$: State in $M$ with no outgoing transitions
\item $N$: Number of states to ``allocate'' as descendants of state $q$
\item $s=\langle{}x,y\rangle{}$: The input read so far
\end{itemize}
Output:
\begin{itemize}
\item $M'$: The completed DFA
\item $N'$: Number of leftover states
\end{itemize}
\vfill
\end{frame}

\begin{frame}{Procedure: BuildDFA (continued)}
\begin{enumerate}
\item Calculate accept probability $p_{s,i}$ given input so far for each possible input $i\in{}\{\langle{}0,0\rangle{},\langle{}0,1\rangle{},\langle{}1,0\rangle{},\langle{}1,1\rangle{}\}$.
\item Define uncertainty $u_i=1-2\left|p_{s,i}-0.5\right|$ for each input $i$.
\item Let ${ n_i=\left\lfloor{}\left(u_i/\sum_iu_i\right)\times{}N\right\rfloor{} }$ be the number of states to allocate to input $i$.
%Assign leftovers to inputs with the highest uncertainty $u_i$.
\item Sort inputs in order of increasing uncertainty $u_i$; for each $i$, do:
\begin{itemize}
\item If $n_i=0$, then add a transition on input $i$ from state $q$ to:
\begin{itemize}
\item \textbf{accept} if $p_i$ is closer to 1 than to 0.
\item \textbf{reject} if $p_i$ is closer to 0 than to 1.
\end{itemize}
\item If $n_i>0$, then do:
\begin{enumerate}
\item Add a new state $q'$ to $M$; transition from $q$ to $q'$ on input $i$.
\item Recursively invoke \textbf{BuildDFA} procedure with DFA $M$, state $q'$, and number of states $(n_i-1)$.
\end{enumerate}
%\item Note: Leftover states after each iteration should be allocated at the following iteration. Leftover states on the last iteration will not be allocated.
\end{itemize}
\end{enumerate}
\vfill % Makes a tiny difference.
\end{frame}

\subsection{Calculating ``Acceptance'' Probabilities}

\begin{frame}{Calculating ``Acceptance'' Probabilities}
%Input length is unspecified, so
Infinitely many possibilities given input-so-far. How do we calculate a probability over this space?

\vspace{0.5cm}
\textit{Equivalent problem:}

Consider the input ${ \langle{}x_nx_{n-1}\ldots{}x_i,y_ny_{n-1}\ldots{}y_i\rangle{} }$ 
%(${ n-i+1 }$ pairs of bits), 
and define
\vspace{-0.1cm}
\begin{align*}
x_1 &= 0.x_nx_{n-1}\ldots{}x_i \\
x_2 &= x_1 + 0.\underbrace{00\ldots{}01}_{\hidewidth{}(n-i+1)\text{ bits}}.
\end{align*}
The ``true'' $x$ (interpreted as a fraction) satisfies ${ x_1\leq{}x<x_2 }$.

(Same result for $y$: ${ y_1\leq{}y<y_2 }$.)

\vspace{0.4cm}
Now, we ask: Is ${ xy\geq{}0.1 }$?
\vfill
\end{frame}

\begin{frame}{Calculating ``Acceptance'' Probabilities}
Calculating the exact probability is equivalent to finding an area!
\begin{center}
\begin{tikzpicture}[scale=2]
\draw[fill=gray!50, dashed] plot[smooth, samples=100, domain=1:2] (\x,1/\x) -| (2,1) -- cycle;
\draw plot[smooth, samples=100, domain=0.7:2.4] (\x,1/\x);
\node[font=\small] at (2.7,0.6) {$xy=0.1$};
\draw (0.2,-0.5) -- (2.3,-0.5);
\node[font=\small] at (0.0,-0.5) {$y_1$};
\draw[dashed] (0.2,1) -- (2.3,1);
\node[font=\small] at (0.0,1) {$y_2$};
\draw (0.5,-0.8) -- (0.5,1.3);
\node[font=\small] at (0.5,-1.0) {$x_1$};
\draw[dashed] (2,-0.8) -- (2,1.3);
\node[font=\small] at (2.0,-1.0) {$x_2$};
\end{tikzpicture}
\end{center}
\vfill
\end{frame}

\section{Next Steps}

\begin{frame}{Next Steps}
Evaluate the method's usefulness:
\begin{itemize}
\item Gain insight about error bounds of automata constructed by this method.
\item Scale up to more complicated problems, such as performing actual multiplication.
\end{itemize}
Improve the method itself:
\begin{itemize}
\item Define a more principled measure of ``uncertainty.''
% (and thus a better way to allocate states to the right subproblems).
\item Consider framing state allocation as a search problem.
\end{itemize}
\end{frame}

\end{document}
