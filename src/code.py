import itertools, math
from PADS import Automata


ALPHABET = {(0, 0), (0, 1), (1, 0), (1, 1)}


def Prob(s):
  bottom = 0.0
  left = 0.0
  placeCtr = 1.0
  for a, b in s:
    bottom += placeCtr * b
    left += placeCtr / 2 * a
    placeCtr /= 2
  top = bottom + placeCtr * 2
  right = left + placeCtr
  totalArea = placeCtr * placeCtr * 2
  if bottom * left >= 1:
    return 1.0
  if top * right <= 1:
    return 0.0
  if top * left >= 1 and bottom * right <= 1:
    return ((right - left) * top - math.log(right / left)) / totalArea
  if top * left < 1 and bottom * right >= 1:
    return (right * (top - bottom) - math.log(top / bottom)) / totalArea
  if top * left < 1 and bottom * right < 1:
    return (- 1 + top * right - math.log(top * right)) / totalArea
  if top * left >= 1 and bottom * right > 1:
    return 1 - (- 1 + bottom * left - math.log(bottom * left)) / totalArea


def AllocateStates(dfa, q, s, N, depth):
  error = 0.0
  probAccept = {}
  uncertainty = {}
  alloc = {}
  for i in ALPHABET:
    probAccept[i] = Prob(s + [i])
    uncertainty[i] = 1 - 2 * abs(probAccept[i] - 0.5)
  if sum(uncertainty.values()) == 0:
    return (N, 0.0)
  unallocated = N
  for i in ALPHABET:
    alloc[i] = int(N * uncertainty[i] / sum(uncertainty.values()))
    unallocated -= alloc[i]
  for i in sorted(uncertainty, key=uncertainty.get):
    if uncertainty[i] != 0:
      alloc[i] += unallocated
    if alloc[i] == 0:
      if probAccept[i] >= 0.5:
        dfa.transitionTable[q, i] = 'accept'
        error += (1 - probAccept[i]) / 4 ** (depth + 1)
      else:
        dfa.transitionTable[q, i] = 'reject'
        error += probAccept[i] / 4 ** (depth + 1)
      maxDepth = depth
    else:
      newState = str(s + [i])
      dfa.transitionTable[q, i] = newState
      (unallocated, newMaxDepth, newError) = AllocateStates(dfa, newState,
                                                            s + [i],
                                                            alloc[i] - 1,
                                                            depth + 1)
      error += newError
      if newMaxDepth > maxDepth:
        maxDepth = newMaxDepth
  return (unallocated, maxDepth, error)


def BuildDFA(N):
  dfa = Automata.DFA()
  dfa.alphabet = ALPHABET
  dfa.initial = 'start'
  dfa.isfinal = lambda st: st == 'accept'
  dfa.transitionTable = {(q, i): q
                         for q in ['accept', 'reject']
                         for i in ALPHABET}
  (leftover, maxDepth, error) = AllocateStates(dfa, 'start', [], N - 3, 0)
  dfa.transition = lambda st, a: dfa.transitionTable[st, a]
  return (dfa, maxDepth, error)


def TestString(s):
  y = 0.0
  x = 0.0
  placeCtr = 1.0
  for a, b in s:
    y += placeCtr * b
    x += placeCtr / 2 * a
    placeCtr /= 2
  return x * y >= 1


if __name__ == '__main__':
  print 'N,Error'
  for N in range(5, 505, 5):
    dfa, maxDepth, error = BuildDFA(N)
    print '{0},{1:e}'.format(N, error)

  print 'N,InputLength,Error'
  for N in [10, 20, 50]:
    dfa, maxDepth, error = BuildDFA(N)
    for inputLen in range(1, 11):
      incorrect = sum(dfa(s) != TestString(s)
                      for s in itertools.product(ALPHABET, repeat=inputLen))
      print '{0},{1},{2:e}'.format(N, inputLen, incorrect / 4.0 ** inputLen)
