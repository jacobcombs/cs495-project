\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{color}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{parskip}
\usepackage{subcaption}
\usepackage{tikz}

\usepackage{listings}
\lstset{
  basicstyle=\ttfamily\footnotesize,
  frame=single,
  numbers=left,
  numberstyle=\tiny,
  xleftmargin=2em
}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty} % Not available in TeXLive 2013?
\allsectionsfont{\sffamily\mdseries\upshape}

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles]{tocloft}
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape}
\title{Design of Approximately Correct Finite Automata for Non-regular Languages}
%\\Special Project: CS 495}
\author{Jacob Combs\\Advisor: Dr.\ Balasubramanian Ravikumar}
\date{\today}

\newcommand{\String}[1]{\texttt{#1}}  % Macro for typesetting strings.

\renewcommand{\arraystretch}{1.25}  % Increase tabular vertical space.

\newcommand{\Square}[2]{
% Tikz macro for making a square for the intersection figures.
% Arguments are the bottom-left corner of the square.
\draw         (#1-.2, #2) -- (#1+1.2, #2);     \node[font=\small] at (#1-.35, #2) {$y_1$};
\draw[dashed] (#1-.2, #2+1) -- (#1+1.2, #2+1); \node[font=\small] at (#1-.35, #2+1) {$y_2$};
\draw         (#1, #2-.2) -- (#1, #2+1.2);     \node[font=\small] at (#1, #2-.35) {$x_1$};
\draw[dashed] (#1+1, #2-.2) -- (#1+1, #2+1.2); \node[font=\small] at (#1+1, #2-.35) {$x_2$};
}


\begin{document}
\maketitle
\tableofcontents

%{\color{red} Placeholder citation~\cite{placeholder}.}

\section{Introduction}
\label{sec:intro}

Given a regular language, it is always possible to construct a finite automaton that recognizes the language. On the other hand, no finite automaton exists that can recognize a non-regular language. However, there may be situations where finite automata that \emph{approximately} recognize non-regular languages are useful. 

In this report, we propose a method to construct finite automata that are capable of approximately recognizing a specific non-regular language. The rest of the report is laid out as follows: Section~\ref{sec:language} defines the non-regular language; Section~\ref{sec:algorithm} describes an algorithm to construct finite automata that approximately recognize this language; Section~\ref{sec:implementation} documents an implementation of this algorithm; and Section~\ref{sec:results} presents results that follow from this implementation.

\section{The Non-regular Language}
\label{sec:language}

Let $a$ and $b$ be strings of \String{0}'s and \String{1}'s having equal, arbitrary length. That is, ${ a=a_na_{n-1}\ldots{}a_1 }$ and ${ b=b_nb_{n-1}\ldots{}b_1 }$ where ${ a_i,b_i\in\{\String{0},\String{1}\} }$. Notice that $a$ and $b$ represent binary numbers, and recall that the product of two $n$-bit numbers is either ${ 2n }$ or ${ 2n-1 }$ bits in length. We would like to design an automaton that processes a pair ${ \langle{}a,b\rangle{} }$ of such input strings and accepts if and only if the product ${ a\times{}b }$ has ${ 2n }$ bits (or equivalently, if and only if ${ (a\times{}b)_{2n}=1 }$).

We represent pairs of strings ${ \langle{}a,b\rangle{} }$ using tuples of corresponding bit:
\[
\begin{pmatrix}a_n\\b_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_1\\b_1\end{pmatrix},
\quad\text{where $a_i,b_i\in\{\String{0},\String{1}\}$ for $i=1,2,\ldots,n$}.
\]

The language $L$ is given by
\[
L = 
\left\{
\begin{pmatrix}a_n\\b_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_1\\b_1\end{pmatrix} :
%\langle{}a,b\rangle{} :
\text{$a=a_na_{n-1}\ldots{}a_1$ and $b=b_nb_{n-1}\ldots{}b_1$ yield a $2n$-bit product}
\right\}.
\]


\section{Algorithm to Construct Approximately Correct Finite Automata}
\label{sec:algorithm}

It can be shown that language $L$ (introduced in Section~\ref{sec:language}) is non-regular, which means there is no finite automaton that accepts only the strings in $L$ with 100\% accuracy. However, we may be able to design an approximately correct finite automaton, or one that produces correct output on most inputs. In this section, we describe a method to produce such approximately correct finite automata for language $L$, given a finite restriction on the number of states $N$.

In this section, we present a method for calculating the probability that an input string is in $L$ given only a prefix of the string (Section~\ref{sec:probability}), and we describe a simple method that uses these probabilities to produce an automaton (Section~\ref{sec:allocation}) that approximately recognizes $L$.

\subsection{Calculating the Probability of Accepting a String}
\label{sec:probability}

In the course of reading an input string $s$, we will have processed a prefix $s'$ of $s$. It is useful to know the probability that ${ s\in L }$ given only this prefix.

Consider the input ${ s=\langle{}a,b\rangle }$, and suppose ${ s'=\langle{}a',b'\rangle }$ is a prefix of $s$ containing the first $k$ symbols of $s$; that is, the prefix $s'$ is of the form
\[
\begin{pmatrix}a_n\\b_n\end{pmatrix}
\begin{pmatrix}a_{n-1}\\b_{n-1}\end{pmatrix}
\cdots
\begin{pmatrix}a_{n-k+1}\\b_{n-k+1}\end{pmatrix}.
\]

Now, instead of treating ${ a'=a_na_{n-1}\ldots{}a_{n-k+1} }$ and ${ b'=b_nb_{n-1}\ldots{}b_{n-k+1} }$ as binary representations of whole numbers, define rational numbers $x'$ and $y'$ using the symbols of $a'$ and $b'$:
\begin{align*}
x' &= 0.a_na_{n-1}\ldots{}a_{n-k+1} &
%&\qquad\text{and}\qquad &
y' &= b_n.b_{n-1}b_{n-2}\ldots{}b_{n-k+1}.
\end{align*}
Suppose that $x$ and $y$ represent rational numbers defined this way for the \emph{complete} input $s$. Then ${ s\in{}L }$ if and only if ${ xy\geq1 }$.

Now, define $x_1$, $x_2$, $y_1$, and $y_2$ by
\begin{align*}
x_1 &= x' &
y_1 &= y' \\
x_2 &= x' + 0.\underbrace{00\ldots{}01}_{\hidewidth{}k\text{ bits}} &
x_2 &= y' + 0.\underbrace{00\ldots{}01}_{\hidewidth{}(k-1)\text{ bits}}.
\end{align*}
Notice that the ``true'' numbers $x$ and $y$ are bounded by ${ x_1\leq{}x<x_2 }$ and ${ y_1\leq{}y<y_2 }$. In fact, the smallest possible values of $x$ and $y$ are $x_1$ and $y_1$ respectively (if all remaining bits are \String{0} symbols); furthermore, $x_2$ and $y_2$ are the smallest possible numbers such that ${ x<x_2 }$ and ${ y<y_2 }$ are necessarily true (the remaining bits may be any number of \String{1} symbols). Therefore, the probability of ${ s\in{}L }$ is given by the area of the intersection of ${ xy\geq1 }$ and the rectangle bounded horizontally by $x_1$ and $x_2$ and vertically by $y_1$ and $y_2$, divided by the rectangle's area.

Finding this area is a calculus problem. First, notice that ${ xy=1 }$ is, in terms of $y$, a strictly decreasing function of $x$ (for ${ x>0 }$). We can identify the trivial cases where the probability is either 0 or 1 by checking the conditions presented in Table~\ref{tab:trivial}. 

\begin{table}
\begin{center}
\begin{tabular}{cc}
\hline
Condition & Probability \\
\hline
$x_2y_2\leq1$ & 0 \\
$x_1y_1\geq1$ & 1 \\
\hline
\end{tabular}
\end{center}
\caption{Probability that ${s\in{}L }$: trivial cases and identifying conditions.}
\label{tab:trivial}
\end{table}

For probabilities not equal to 0 or 1, it is useful to consider the four cases illustrated in Figure~\ref{fig:cases}. Formulas for the probabilities corresponding to each case are presented in Table~\ref{tab:cases}.

\begin{figure}
\centering
\begin{subfigure}{.35\linewidth}\centering
\begin{tikzpicture}[scale=2.5]
\fill[gray!50] plot[smooth, samples=100, domain=1.3:1.3+1] (\x,1/\x) -| (1.3+1,.1+1) -- (1.3,0.1+1) -- cycle;
\draw plot[smooth, samples=100, domain=1.3-.2:1.3+1+.2] (\x,1/\x);
\node[font=\small] at (1.3+1+.3,0.1+.45) {$xy\geq1$};
\Square{1.3}{.1}
\end{tikzpicture}
\caption{Case 1: Top and bottom sides.}
\label{fig:cases-1}
\end{subfigure}
\begin{subfigure}{.35\linewidth}\centering
\begin{tikzpicture}[scale=2.5]
\fill[gray!50] plot[smooth, samples=100, domain=1/(1.3+1):1/1.3] (\x,1/\x) -| (.1+1,1.3) -- (.1+1,1.3+1) -- cycle;
\draw plot[smooth, samples=100, domain=1/(1.3+1+.2):1/(1.3-.2)] (\x,1/\x);
\node[font=\small] at (0.1+.5,1.3-.2) {$xy\geq1$};
\Square{.1}{1.3}
\end{tikzpicture}
\caption{Case 2: Left and right sides.}
\label{fig:cases-2}
\end{subfigure}
\\[.5cm]
\begin{subfigure}{.35\linewidth}\centering
\begin{tikzpicture}[scale=2.5]
\fill[gray!50] plot[smooth, samples=100, domain=.28+.5:.28+1] (\x,1/\x) -| (.28+1,.28+1) -- cycle;
\draw plot[smooth, samples=100, domain=1/(.28+1+.2):.28+1+.2] (\x,1/\x);
\node[font=\small] at (.28+1+.3,.28+.55) {$xy\geq1$};
\Square{.28}{.28}
\end{tikzpicture}
\caption{Case 3: Top and right sides.}
\label{fig:cases-3}
\end{subfigure}
\begin{subfigure}{.35\linewidth}\centering
\begin{tikzpicture}[scale=2.5]
\fill[gray!50] plot[smooth, samples=100, domain=.78:.78+.5] (\x,1/\x) -| (.78+1,.78) -- (.78+1,.78+1) -- (.78,.78+1) -- cycle;
\draw plot[smooth, samples=100, domain=.78-.2:1/(.78-.2)] (\x,1/\x);
\node[font=\small] at (.78+.5,.78-.25) {$xy\geq1$};
\Square{.78}{.78}
\end{tikzpicture}
\caption{Case 4: Bottom and left sides.}
\label{fig:cases-4}
\end{subfigure}
\caption{Intersection of ${ xy\geq1 }$ and bounding rectangles. Four cases are nontrivial.}
\label{fig:cases}
\end{figure}

\begin{table}
\begin{center}
\begin{tabular}{ccc}
\hline
Case & Condition & Area \\
\hline
1 & $x_1y_2\geq1$ and $x_2y_1\leq1$ & $\left[(x_2-x_1)y_2-\ln(x_2/x_1)\right]/A$  \\
2 & $x_1y_2<1$ and $x_2y_1\geq1$ & $\left[x_2(y_2-y_1)-\ln(y_2/y_1)\right]/A$     \\
3 & $x_1y_2<1$ and $x_2y_1<1$ & $\left[-1+x_2y_2-\ln{}x_2y_2\right]/A$            \\
4 & $x_1y_2\geq1$ and $x_2y_1>1$ & $1-\left[-1+x_1y_1-\ln{}(1/(x_1y_1))\right]/A$ \\
\hline
\end{tabular}
\end{center}
\caption{Probabilities by case with identifying conditions. $A$ represents the area of the bounding rectangle: ${ A = (x_2-x_1)(y_2-y_1) = 2^{-k(k-1)} }$ where $k$ is the number of input symbols read so far.}
\label{tab:cases}
\end{table}

For Mathematica code that can be used to verify the area formulas presented in Table~\ref{tab:cases}, see Appendix~\ref{sec:code}.


\subsection{Constructing the Automaton}
\label{sec:allocation}

In this section, we describe a procedure that takes as input a maximum number of states $N$ and builds a finite automaton that approximately recognizes language $L$.

The procedure is defined recursively; its inputs are:
\begin{itemize}
\item $M$, an incomplete finite automaton. $M$ will initially have 3 states:
\begin{itemize}
\item The \textbf{start} state initially has no outgoing transitions.
\item The \textbf{accept} state is absorbing.
\item The \textbf{reject} state is absorbing.
\end{itemize}
\item $q$, a state in $M$ with no outgoing transitions. Initially, ${ q=\textbf{start} }$.
\item $N$, the maximum number of states that may be created as descendants of state $q$.
\item $s'$, an input string leading to state $q$; $s'$ may be considered a prefix of the entire input string $s$. Initially, ${ s'=\epsilon }$, the empty string.
\end{itemize}
The outputs are:
\begin{itemize}
\item $M$, a finite automata for which the outgoing transitions from state $q$, and from any new descendant states of $q$, are defined.
\item $N'$, the number of unused states.
\end{itemize}

The steps of the procedure are outlined below.
\begin{enumerate}
\item For each input symbol $i$, do:
\begin{enumerate}
\item Calculate $p_i$, the probability that ${ s\in{}L }$ given the prefix ${ s'i }$ (${ s'i }$ refers to the concatenation of $s'$ and $i$). The method for calculating this probability is described in Section~\ref{sec:probability}.
\item Define a measure of uncertainty $u_i\gets1-2\left|p_i-0.5\right|$.
\item Calculate $n_i$, the number of states to ``allocate'' to input $i$. Let ${ n_i\gets\left\lfloor{}\left(u_i/\sum_iu_i\right)\times{}N\right\rfloor{} }$, to allocate a higher number of states to inputs with higher uncertainty.
\end{enumerate}
\item Let ${ N'\gets0 }$ represent the number of unused states.
\item Sort input symbols in order of least-to-greatest uncertainty $u_i$. For each input symbol $i$, do:
\begin{itemize}
\item Allocate any leftover states to $i$: If ${ N'>0 }$, then let ${ n_i\gets{}n_i+N' }$.
\item If ${ n_i=0 }$, add an outgoing transition on input $i$ from state $q$ to:
\begin{itemize}
\item \textbf{accept} if ${ p_i\geq0.5 }$ (that is, if $p_i$ is closer to 1 than 0).
\item \textbf{reject} if ${ p_i<0.5 }$.
\end{itemize}
\item If ${ n_i>0 }$, do:
\begin{enumerate}
\item Add a new state $q'$ to $M$; add a transition from $q$ to $q'$ on input $i$.
\item Recursively invoke the procedure with the following inputs: automaton $M$, state $q'$, number of states $(n_i-1)$, and input so far ${ s'i }$. (After the recursive invocation, $q'$ and all its descendant states will have outgoing transitions defined for each input symbol.)
\end{enumerate}
\end{itemize}
\item Output the automaton $M$ and the number of unused states $N'$.
\end{enumerate}


\section{Implementation}
\label{sec:implementation}

This implementation is in Python, and utilizes David Eppstein's \texttt{PADS} library for automata processing. Input strings are encoded as sequences of tuples of binary digits---see the \texttt{ALPHABET} global variable in Listing~\ref{lst:imports}.
\lstinputlisting[float,
  linerange=1-5, firstnumber=1,
  caption={Imports and helpful global constants.},
  captionpos=b,
  label={lst:imports},
]{../src/code.py}

The probability calculation method described in Section~\ref{sec:probability} is implemented by the code presented in Listing~\ref{lst:probability}.
\lstinputlisting[float,
  linerange=8-30, firstnumber=8,
  caption={Implementation of probability calculation described in Section~\ref{sec:probability}.},
  captionpos=b,
  label={lst:probability},
]{../src/code.py}

The automaton construction algorithm described in Section~\ref{sec:allocation} is implemented by the code in Listing~\ref{lst:allocation}. Among the diagnostic statistics that this code outputs is the theoretical error of the automaton.
\lstinputlisting[float,
  linerange=33-81, firstnumber=33,
  caption={Implementation of DFA construction algorithm presented in Section~\ref{sec:allocation}.},
  captionpos=b,
  label={lst:allocation},
]{../src/code.py}

A useful method to test whether a string is in language $L$ is presented in Listing~\ref{lst:teststring}. This code facilitates empirical verification that the theoretical error output by the code from Listing~\ref{lst:allocation} is reasonable (see Section~\ref{sec:results}).
\lstinputlisting[float,
  linerange=84-92, firstnumber=84,
  caption={Method to test whether a string is in language $L$.},
  captionpos=b,
  label={lst:teststring},
]{../src/code.py}

Listing~\ref{lst:data} presents code that uses these methods to produce data, which is analyzed in Section~\ref{sec:results}.
\lstinputlisting[float,
  linerange=95-107, firstnumber=95,
  caption={Code to generate data related to DFA error rates.},
  captionpos=b,
  label={lst:data},
]{../src/code.py}

\section{Results}
\label{sec:results}

The implementation of the allocation algorithm presented in Section~\ref{sec:implementation} outputs a measure of the error of produced automata. To verify that this measure is reasonable, we examine empirical error rates for automata of ${ N=10,20,50 }$ states for all input strings of length ${ 1,2,\ldots,10 }$; these are plotted in Figure~\ref{fig:inputlengths}.
\begin{figure}
\begin{center}
\includegraphics[width=.9\textwidth]{../results/inputLengths}
\caption{Empirical error rates plotted on length of input string for each of three automata with different state counts (${ N=10,20,50 }$). Horizontal lines represent the theoretical error rate.}
\label{fig:inputlengths}
\end{center}
\end{figure}

Figure~\ref{fig:statecounts} shows the relationship between the maximum number of states $N$ and the accuracy of the produced automaton. As we might expect, the algorithm produces more accurate automata for larger values of $N$. The 10-state automaton is incorrect given any of about $ 1.6\% $ of all inputs; the 50-state automaton, about $ 0.35\% $; and the 500-state automaton, about $ 0.055\% $.
\begin{figure}
\begin{center}
\includegraphics[width=.9\textwidth]{../results/stateCounts100}
%\includegraphics[width=.9\textwidth]{../results/stateCounts500}
\caption{Automata error rate on number of states. Given more states, the algorithm produces an automaton that more accurately recognizes language $L$.}
\label{fig:statecounts}
\end{center}
\end{figure}


\begin{appendix}
\section{Supporting Mathematica Code}
\label{sec:code}

Mathematica code used to calculate the area formulas presented in Table~\ref{tab:cases} is given in Listing~\ref{lst:mathematica}.

\vspace{\parskip}
\begin{lstlisting}[float,
  caption={Supporting Mathematica code.},
  label={lst:mathematica}
  ]
(* Case 1 *)
Assuming[0 < x1 < x2 && 0 < y1 < y2,
  Integrate[y2 - 1/x, {x, x1, x2}] / A
]

(* Case 2 *)
Assuming[0 < x1 < x2 && 0 < y1 < y2,
  Integrate[x2 - 1/y, {y, y1, y2}] / A
]

(* Case 3 *)
Assuming[0 < x1 < x2 && 0 < y1 < y2 && x2 * y2 > 1,
  Integrate[y2 - 1 / x, {x, 1 / y2, x2}] / A
]

(* Case 4 *)
Assuming[0 < x1 < x2 && 0 < y1 < y2 && x1 * y1 > 1,
  1 - Integrate[1 / x - y1, {x, x1, 1 / y1}] / A
]
\end{lstlisting}
\end{appendix}


%\bibliographystyle{acm}
%\bibliography{cs495-report}

\end{document}
